FROM alpine:3.8
RUN apk update && apk add xz wget git make coreutils gcc bash musl-dev bc perl file \
	tar gawk patch grep unzip bzip2 python2 findutils zlib-dev ncurses-dev shadow \
	diffutils linux-headers shadow diffutils tcl alpine-sdk tcl
RUN useradd -m build
USER build
